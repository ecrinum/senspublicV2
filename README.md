# SENS PUBLIC - v2

## BaseX initial setup

#### Create user

```
CREATE USER sp_api password
```

#### Setup HTML parsing

```
SET PARSER html
SET CREATEFILTER *.html
```

#### Create database

```
CREATE DB sp
GRANT READ ON sp TO sp_api
```

#### Add documents

```
OPEN sp
ADD /data
```
