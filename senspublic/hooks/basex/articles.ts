import { useQuery } from '@tanstack/react-query';
import { getArticles } from 'lib/basex/articles';

export const useArticles = () => {
    return useQuery(['articles'], () => getArticles());
}