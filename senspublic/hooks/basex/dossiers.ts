import { useQuery } from '@tanstack/react-query';
import { getDossiers } from 'lib/basex/dossiers';

export const useDossiers = () => {
    return useQuery(['dossiers'], () => getDossiers());
}