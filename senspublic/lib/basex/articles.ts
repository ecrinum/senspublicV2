import axios from 'axios';

export const getArticles = async () => {
    const { data } = await axios.get("http://localhost:8080/articles.json");
    return data.articles;
}