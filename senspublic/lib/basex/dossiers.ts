import axios from 'axios';

export const getDossiers = async () => {
    const { data } = await axios.get("http://localhost:8080/index/dossiers");
    return data.dossiers;
}