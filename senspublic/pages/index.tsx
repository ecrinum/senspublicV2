import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { dehydrate, QueryClient } from '@tanstack/react-query';
import { getDossiers } from 'lib/basex/dossiers';
import { getArticles } from 'lib/basex/articles';
import { useDossiers } from 'hooks/basex/dossiers'
import { useArticles } from 'hooks/basex/articles'
import { useState } from 'react'

export const getServerSideProps = async () => {
  const queryClient = new QueryClient()
  await queryClient.fetchQuery(['dossiers'], () => getDossiers())
  await queryClient.fetchQuery(['articles'], () => getArticles())

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  }
}

export default function Home() {
  const { data: dossiers, isDossiersLoading } = useDossiers();
  const { data: articles, isArticlesLoading } = useArticles();
  const [searchTerm, setSearchTerm] = useState("");

  if (isDossiersLoading) return <div>Loading....</div>

  // console.log(dossiers)
  // console.log(articles)
  return (
    <>
      <input type="text" onChange={(e) => setSearchTerm(e.target.value)} />
      <div className={styles.container}>
        <h2>Dossiers</h2>
        {
          dossiers.filter((elem) => elem.title.includes(searchTerm)).map((elem) => <pre key={elem.id}>{elem.title}</pre>)
        }
      </div>
      <div className={styles.container}>
        <h2>Articles</h2>
        {
          articles.filter((elem) => elem.title.includes(searchTerm)).map((elem) => <pre key={elem.id}>{elem.title}</pre>)
        }
      </div>
    </>
  )
}
