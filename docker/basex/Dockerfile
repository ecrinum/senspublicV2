FROM maven:3-openjdk-18-slim
LABEL maintainer="Tim G <timoguic@gmail.com>"

RUN apt-get -qy update && apt-get -qy install git && rm -rf /var/lib/apt/lists/*

# Download git archive
RUN curl -sL https://github.com/BaseXdb/basex/archive/refs/tags/10.3.tar.gz | tar -C /usr/src -z -x -f - \
    && ln -s /usr/src/basex-10.3 /usr/src/basex

# Build BaseX
RUN cd /usr/src/basex && mvn clean install -DskipTests

# Add symlinks for BaseX binaries
RUN ln -s /usr/src/basex/basex-*/etc/* /usr/local/bin

# Create user
RUN useradd -m -d /srv -u 1984 basex

# Create folders
RUN mkdir -p /srv/.m2 /srv/basex/data /srv/basex/repo /srv/basex/webapp \
    && cp -r /usr/src/basex/basex-api/src/main/webapp/WEB-INF /srv/basex/webapp \
    && cp -r /usr/src/basex/basex-api/src/main/webapp/dba /srv/basex/webapp

RUN chown -R basex /srv
USER basex
ENV MAVEN_CONFIG=/srv/.m2

EXPOSE 8080 8081

VOLUME ["/srv/basex/data", "/srv/basex/repo", "/srv/basex/webapp"]
WORKDIR /srv

# Run BaseX HTTP server by default
CMD ["/usr/local/bin/basexhttp"]