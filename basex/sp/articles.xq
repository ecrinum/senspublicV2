module namespace page = 'http://basex.org/examples/web-page';

import module namespace utils = 'lib/utils' at 'lib/utils.xqm';

declare
  %rest:path("articles.json")
  %output:method("json")
  %rest:GET
  %rest:produces("application/json")
  function page:all-articles() {
    <json type="object">
      <articles type="array">
      {
        for $elem in db:open("sp")
          where $elem/html/head/meta[@name='DC.type' and not(@content="Sommaire dossier")]
          return
            <_ type="object">
              <id type="string">{
                utils:get-id-from-path(
                  db:path($elem)
                )
              }</id>
              <title type="string">{$elem/html/head/title/text()}</title>
            </_>
      }
      </articles>
    </json>
  };


declare
  %rest:path("articles/type/{$art_type}")
  %rest:GET
  function page:documents-by-type($art_type as xs:string) {
    <response>
        {
		let $docs := db:open("sp")
		for $elem in $docs
		where $elem//head/meta[@name='DC.type' and @content=web:decode-url($art_type)]
		return
            utils:generate-doc($elem)
	}
    </response>
};

declare
  %rest:path("articles/author/{$name}")
  %rest:GET
  function page:documents-by-author($name as xs:string) {
    <response>
        {
		let $docs := db:open("sp")
		for $elem in $docs
		where $elem/html/body/div[@class="indexations-foaf"]//div[
            @vocab='http://xmlns.com/foaf/0.1/'
            and @class='foaf-author'
            and fn:concat(
                span[@property='firstName'],
                ' ',
                span[@property='familyName']
            )=web:decode-url($name)
        ]
		return
            utils:generate-doc($elem)
	}
    </response>
};


