module namespace page = 'http://basex.org/examples/web-page';

import module namespace utils = 'lib/utils' at 'lib/utils.xqm';

declare
  %rest:path("article/{$id}/path")
  %rest:GET
  function page:article($id as xs:string) {
    let $node := db:open("sp")/html/head/meta[
        @name="DC.identifier"
        and (
            contains(@content, concat("article", $id, ".html"))
            or contains(@content, concat("/articles/", $id))
        )
        ]
    let $path := db:path($node)
    return
        <path>{$path}</path>
};

declare
  %rest:path("types")
  %rest:GET
  function page:article-types() {
    <response>
    {
    for $label in fn:distinct-values(fn:for-each(
		db:open("sp")/html/head/meta[@name="DC.type" and @class="typeArticle" and not(contains(@content, "Sommaire"))]/data(@content),
        fn:lower-case(?)
    )) return
        <type>{$label}</type>
    }
    </response>
};

declare
  %rest:path("article/{$id}/logo")
  %rest:GET
  function page:logo-url($id) {
  <response>{utils:get-logo-path($id)}</response>
};
