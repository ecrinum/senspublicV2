module namespace utils = 'lib/utils';

declare function utils:get-id-from-path($path as xs:string) as xs:string {
    fn:replace($path, "SP\d{2,4}/SP(\d{2,4}).html", "$1")
};

declare function utils:generate-doc($elem as node()) {
    let $article-id := utils:get-id-from-path(db:path($elem))
    return
    <document
        id="{$article-id}"
        logoUrl="{utils:get-logo-path($article-id)}"
    >
        <title>{$elem//head/title/text()}</title>
    </document>
};

declare function utils:get-logo-path($article-id as xs:string) {
    let $dir := "/data/articles/SP" || $article-id || "/media"
    return if (file:is-dir($dir)) then
        for $name in file:list($dir)
            where starts-with($name, "arton" || $article-id)
            return text{"SP" || $article-id || "/media/" || $name}
     else ""
    };
