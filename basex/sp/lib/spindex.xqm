module namespace spindex = 'lib/spindex';

import module namespace utils = 'lib/utils' at 'utils.xqm';

declare variable $spindex:DB := (db:open("sp")/html);

declare function spindex:get-authors() {
	let $docs := $spindex:DB/body/div[@class="indexations-foaf"]//div[@vocab='http://xmlns.com/foaf/0.1/' and @class='foaf-author' and span[@property='familyName']/text() != ""]
    let $names := fn:distinct-values(
        fn:for-each($docs, function($elem) {
            fn:concat($elem/span[@property='firstName'], ' ', $elem/span[@property='familyName'])
        })
    )
    return
        for $name in $names
        let $author := fn:head($docs[fn:concat(span[@property='firstName'], ' ', span[@property='familyName']) = $name])
        return
            <author
                name="{$name}"
                openid="{$author/span[@property='openid']}"
            />
};

declare function spindex:get-author-keywords() {
    (: AUTHOR KEYWORDS :)
    let $flat_auth_keywords := fn:string-join(data($spindex:DB/head/meta[@name='keywords']/@content), ",")
    let $auth_keywords := fn:distinct-values(
        fn:for-each(fn:tokenize($flat_auth_keywords, ","), function($elem) { fn:normalize-space(fn:lower-case($elem)) })
    )
    return
        for $kw in $auth_keywords return
        <keyword
          type="author"
          name="{$kw}"
        />
};

declare function spindex:get-editor-keywords() {
    let $edit_keywords := $spindex:DB/body/div[@id="schema-scholarly-article"]/div[@class='keywords']/div
    let $keywords := fn:distinct-values($edit_keywords/span[@property='subject' and @class='label']/text())
    for $keyword in $keywords
    let $kw := fn:head(
        $edit_keywords[
            span[@property='subject'
            and @class='label']/text() = $keyword
        ]
    )
    return
        <keyword
          type="editor"
          lang="{$kw/data(@lang)}"
          name="{$kw/span[@property='subject' and @class='label']/text()}"
          idRameau="{$kw/span[@class="idRameau" and @property="subject"]/text()}"
          uriRameau="{$kw/span[@class="urlRameau" and @property="subject"]/text()}"
          wikidata="{$kw/span[@class="wikidata" and @property="subject"]/text()}"
        />
};

declare function spindex:get-articles() {
    let $docs := db:open("sp")
    for $elem in $docs
    where $elem/html/head/meta[@name='DC.type' and not(@content="Sommaire dossier")]
    return
        <article id="{utils:get-id-from-path(db:path($elem))}" title="{$elem/html/head/title/text()}" />
};

declare function spindex:get-dossiers() {
    let $docs := db:open("sp")
    for $elem in $docs
        where $elem/html/head/meta[@name='DC.type' and @content="Sommaire dossier"]
        (:~ return <dossier id="{db:path($elem)}" title="{$elem/html/head/title/text()}" /> ~:)
        return $elem
};
