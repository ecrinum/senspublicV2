module namespace page = 'http://basex.org/examples/web-page';

import module namespace spindex = 'lib/spindex' at 'lib/spindex.xqm';

declare
  %rest:path("index/all")
  %rest:GET
  function page:index() {
    <response>
        <authors>
        { spindex:get-authors() }
        </authors>
        <keywords>
        { spindex:get-editor-keywords() }
        { spindex:get-author-keywords() }
        </keywords>
        <articles>
        { spindex:get-articles() }
        </articles>
        <dossiers>
        { spindex:get-dossiers() }
        </dossiers>
   </response>
};

declare
  %rest:path("index/keywords")
  %rest:GET
  function page:keywords() {
    <response>
    <keywords>
    { spindex:get-editor-keywords() }
    { spindex:get-author-keywords() }
    </keywords>
    </response>
};

declare
  %rest:path("index/authors")
  %rest:GET
  function page:authors() {
    <response>
    <authors>
    { spindex:get-authors() }
    </authors>
    </response>
};

declare
  %rest:path("index/dossiers")
  %output:method("json")
  (:~ %output:json("format=attributes") ~:)
  %rest:GET
  %rest:produces("application/json")
  function page:dossiers() {
    <json type="object">
    <dossiers type="array">
    { for $dossier in spindex:get-dossiers()
        return
          <_ type="object">
            <id type="string">{db:path($dossier)}</id>
            <title type="string">{$dossier/html/head/title/text()}</title>
          </_>
    }
    </dossiers>
    </json>
};
