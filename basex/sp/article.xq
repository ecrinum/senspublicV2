module namespace page = 'http://basex.org/examples/web-page';

declare
  %rest:path("article/{$id}")
  %rest:GET
  function page:article($id as xs:string) {
    let $html := db:open("sp", fn:concat("SP", fn:concat($id, ".html")))/html
    let $article := $html/body/article
    return
    <article>
        {$html/head/title}
        <contents>
        {
            copy $updated := $article
            modify (
                for $paragraph at $position in $updated/section/p
                where not(fn:starts-with($updated/../@id, "ref")) and not(fn:starts-with($updated/../@id, "fn"))
                return (
                    insert node attribute index{$position} into $paragraph,
                    insert node <span class="paginati">{$position}</span> as first into $paragraph
                )
            )
            return $updated/section[not(contains(@class, "footnotes")) and not(fn:starts-with(@id, "biblio"))]
        }
        </contents>
        <footnotes>
        {
            for $note in $article/section[@class='footnotes']/ol/li
            return <note id="{data($note/@id)}">{$note/*}</note>
        }
        </footnotes>
        <bibliographie>
        {
            for $cit in $article//div[@id='refs']/div
            return
                    let $refid := fn:substring-after(data($cit/@id), 'ref-')
                    return
                        <ref id="cleBib/{$refid}" class="cleBib">
                            {$cit/*}
                        </ref>
        }
        </bibliographie>
    </article>
};
