module namespace page = 'http://basex.org/examples/web-page';

declare
  %rest:path("filter_by_author")
  %rest:GET
  %rest:query-param("name", "{$name}")
  function page:by_author($name as xs:string) {
    let $docs := db:open("sp")//head/meta[@name='author' and contains(lower-case(@content), lower-case(web:decode-url($name)))] return
    <response>
        {for $node in $docs return <title value="{db:path($node)}" />}
    </response>
};
